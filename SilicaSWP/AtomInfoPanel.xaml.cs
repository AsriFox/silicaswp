﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 15.11.2021
 * Time: 10:25
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace SilicaSWP
{
	/// <summary>
	/// Interaction logic for AtomInfoPanel.xaml
	/// </summary>
	public partial class AtomInfoPanel : ListBoxItem
	{
		public int RefId { get; private set; }
		public Atom RefAtom { get; private set; }
		Vector RestingPosition;
		
		public int DecimalPlaces { get; set; }
		
		string PosString(double value) {
			int pv = (int)Math.Round(Math.Abs(value) * 4);
			if (pv % 4 == 0)
				return String.Format("{0:F0}", value);
			if (pv % 2 == 0)
				return String.Format("{0:F1}", 0.5 * Math.Round(value * 2));
			return String.Format("{0:F2}", 0.25 * Math.Round(value * 4));
		}
		
		public AtomInfoPanel(Atom SelAtom, int Index) {
			InitializeComponent();
			RefAtom = SelAtom;
			RefAtom.PropertyChanged += UpdateData;
			RefId = Index;
			
			var pos = SelAtom.Position / AtomParams.LATPAR_SI;
			RestingPosition = new Vector(0.25 * Math.Round(pos.x * 4), 0.25 * Math.Round(pos.y * 4), 0.25 * Math.Round(pos.z * 4)) * AtomParams.LATPAR_SI;
			TB_AtomId.Text = String.Format("{0} {1} {2}", PosString(pos.x), PosString(pos.y), PosString(pos.z));
			DecimalPlaces = 3;
			UpdateData(this, new PropertyChangedEventArgs("Position"));
		}
		
		~AtomInfoPanel() {
			RefAtom.PropertyChanged -= UpdateData;
		}
		
		void UpdateData(object sender, PropertyChangedEventArgs e) {
			string format = "{" + String.Format("0:F{0:0}", DecimalPlaces) + "}";
			if (e.PropertyName == "Position") {
				var shift = RefAtom.Position - RestingPosition;
				TB_AtomPosX.Text = String.Format(format, shift.x);
				TB_AtomPosY.Text = String.Format(format, shift.y);
				TB_AtomPosZ.Text = String.Format(format, shift.z);
			}
		}
	}
}