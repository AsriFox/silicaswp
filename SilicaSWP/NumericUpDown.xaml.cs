﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 11/18/2021
 * Time: 12:55
 */
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SilicaSWP
{
	/// <summary>
	/// Interaction logic for NumericUpDown.xaml
	/// </summary>
	public partial class NumericUpDown : UserControl
	{
		decimal _value = 0;
		public IConvertible Value {
			get { return _value; }
			set {
				decimal v = Convert.ToDecimal(value);
				if (v < _minimum)
					_value = _minimum;
				else if (v > _maximum)
					_value = _maximum;
				else _value = v;
				TB_Number.Text = _value.ToString();
			}
		}
		
		decimal _step = 1;
		public IConvertible Step {
			get { return _step; }
			set {
				decimal v = Convert.ToDecimal(value);
				if (v > _maximum - _minimum)
					throw new ArgumentException("Value step should fit in range!");
				_step = v;
			}
		}
		
		decimal _minimum = 0;
		public IConvertible Minimum {
			get { return _minimum; }
			set {
				decimal v = Convert.ToDecimal(value);
				if (v > _maximum)
					throw new ArgumentException("Minimum should be lower than maximum!");
				if (_value < v) {
					_value = v;
					TB_Number.Text = v.ToString();
				}
				_minimum = v;
			}
		}
		
		decimal _maximum = 100;
		public IConvertible Maximum {
			get { return _maximum; }
			set {
				decimal v = Convert.ToDecimal(value);
				if (v < _minimum)
					throw new ArgumentException("Maximum should be higher than minimum!");
				if (_value > v) {
					_value = v;
					TB_Number.Text = v.ToString();
				}
				_maximum = v;
			}
		}
		
		public NumericUpDown()
		{
			InitializeComponent();
		}
		
		void TBNumber_TextChanged(object sender, TextChangedEventArgs e) {
			decimal result;
			if (!Decimal.TryParse(TB_Number.Text, out result)) {
				// System.Media.SystemSounds.Beep.Play();
				Value = _minimum;
				return;
			}
			Value = result;
		}
		
		void BnUp_Click(object sender, RoutedEventArgs e) { 
			Value = _value + _step;
			TB_Number.Focus();
		}
		
		void BnDown_Click(object sender, RoutedEventArgs e) { 
			Value = _value - _step;
			TB_Number.Focus();
		}
		
		void NUD_KeyDown(object sender, KeyEventArgs e) {
			if (!(sender as UIElement).IsFocused) return;
			switch (e.Key) {
				case Key.Up:
					Value = _value + _step;
					return;
				case Key.Down:
					Value = _value - _step;
					return;
			}
		}
		
		void NUD_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			if (e.Delta > 0) 
				Value = _value + _step;
			else 
				Value = _value - _step;
		}
	}
	
	public sealed class ArrowFontSizeConverter : IValueConverter
	{
		public object Convert(object value, Type TargetType, object parameter, System.Globalization.CultureInfo Culture) {
			var fstr = (Boolean.Parse(parameter as string) ? "M 0 {0:0} L {1:0} {0:0} L {0:0} 0 Z" : "M 0 0 L {0:0} {0:0} L {1:0} 0 Z");
			double? size = (double?)value / 3;
			return Geometry.Parse(String.Format(fstr, size, 2 * size));
		}
		
		public object ConvertBack(object value, Type TargetType, object parameter, System.Globalization.CultureInfo Culture) {
			throw new NotSupportedException();
		}
	}
}