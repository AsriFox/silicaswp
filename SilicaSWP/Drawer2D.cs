﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 09/12/2021
 * Time: 16:21
 */
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SilicaSWP
{
	/// <summary>
	/// Класс функций двумерного рисования.
	/// </summary>
	public class Drawer2D
	{
		/// <summary>
		/// Рабочая область изображения.
		/// </summary>
		public Rect Region;
		
		/// <summary>
		/// Коллекция элементов изображения.
		/// </summary>
		DrawingCollection Children;
		
		/// <summary>
		/// Фактор преобразования изображения.
		/// </summary>
		public Transform Transform;
		
		public double dt;
		
		/// <summary>
		/// Толщина линий.
		/// </summary>
		public double w { get { return 0.1 * Region.Height / 30; } }
		
		public Drawer2D() { Children = new DrawingCollection(); }
				
		/// <summary>
		/// Создание объекта рисования - версия для графиков.
		/// </summary>
		/// <param name="R">Координатный регион, в котором происходит рисование.</param>
		/// <param name="N">Количество линий сетки.</param>
		/// <returns>Объект DrawingGroup.</returns>
		public static Drawer2D Graph(Rect R, int N = 10, double dt = 1) {
			var Gr = new Drawer2D() { 
				Region = R, 
				Transform = new ScaleTransform(1, -1),
				dt = dt
			};
			
			// Фон:
			Gr.Children.Add(new GeometryDrawing(Brushes.Beige, null, new RectangleGeometry(R)));
			
			// Линии сетки ординат:
			var GLines = new GeometryGroup();
			for (int i = 0; i <= N; i++)
                GLines.Children.Add(new LineGeometry(
                    new Point(R.Left, R.Top + i * R.Height / N),
                    new Point(R.Right, R.Top + i * R.Height / N)
                ));
            Gr.Children.Add(new GeometryDrawing(Brushes.Gray, new Pen(Brushes.Gray, Gr.w), GLines));
            // Линия координатной оси абсцисс:
            if (R.Top <= 0.0 && R.Bottom >= 0.0)
				Gr.Children.Add(new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 2 * Gr.w),
	                    new LineGeometry(new Point(R.Left, 0.0), new Point(R.Right, 0.0))));            
            
			// Подписи меток на осях:
//			var GLabls = new GeometryGroup();
//			for (int i = 0; i <= N; i++)
//			{
//				var Labl = new FormattedText(
//					String.Format("{0:F1}", -i * 0.2),
//					System.Globalization.CultureInfo.InvariantCulture, FlowDirection.LeftToRight,
//					new Typeface("Verdana"), 1.5 * Gr.w, Brushes.Black, 1.0);
//
//				GLabls.Children.Add(Labl.BuildGeometry(new Point(R.Left - 0.2 * R.Width, R.Top + i * R.Height / N - Gr.w)));
//			}
//			GLabls.Transform = new ScaleTransform(1, -1);
//			Gr.Children.Add(new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, Gr.w), GLabls));
            
            return Gr;
		}
		
		/// <summary>
		/// Получение готового изображения.
		/// </summary>
		public DrawingImage GetImage() {
			var Gr = new DrawingGroup {
				Children = Children,
				Transform = Transform
			};
			return new DrawingImage(Gr);
		}
		
		/// <summary>
		/// Нарисовать ломаную линию.
		/// </summary>
		/// <param name="Line">Координаты точек линии.</param>
		/// <param name="LineColor">Кисть цвета линии.</param>
		public void AddLine(IList<Point> Line, Brush LineColor) {
	        var SubGr = new StreamGeometry();
	        using (var GC = SubGr.Open())
	        {
	            GC.BeginFigure(Line[0], false, false);
	            for (int i = 1; i < Line.Count; i++)
	                GC.LineTo(Line[i], true, false);
	            GC.Close();
	        }
	        SubGr.Freeze();
			Children.Add(new GeometryDrawing(LineColor, new Pen(LineColor, 3 * w), SubGr));
		}
		
		/// <summary>
		/// Нарисовать на поле единственный график стандартного цвета.
		/// </summary>
		/// <param name="Line">Координаты точек графика.</param>
		public void AddGraph(IList<Point> Line) {
			// Линии сетки абсцисс:
			double tm = Line[Line.Count - 1].X;
			var GLines = new GeometryGroup();
			for (double t = 0; t <= tm; t += dt)
                GLines.Children.Add(new LineGeometry(
					new Point(Region.Left + Region.Width * t / tm, Region.Bottom),
                    new Point(Region.Left + Region.Width * t / tm, Region.Top)
                ));
            Children.Add(new GeometryDrawing(Brushes.Gray, new Pen(Brushes.Gray, w), GLines));
			
			AddLine(Line, Brushes.Blue);
		}
	}
}
