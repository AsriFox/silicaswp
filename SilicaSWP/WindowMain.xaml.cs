﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 12.09.2021
 * Time: 13:50
 */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SilicaSWP
{
	/// <summary>
	/// Логика взаимодействия с WindowMain.xaml
	/// </summary>
	public partial class WindowMain : Window
	{
		Relaxation LatProc;
		double EnergyScale, EnergyAsympt;
		
		public WindowMain()
		{
			InitializeComponent();

            LatProc = new Relaxation();
            LatProc.Tick += Phys_Calculate;
            
            Action<System.Windows.Forms.DataVisualization.Charting.Chart> SetupChartArea = (chart) => {
				chart.ChartAreas.Clear();
	            chart.ChartAreas.Add("Default");
	            chart.ChartAreas["Default"].AxisX.Minimum = 0;
	            chart.ChartAreas["Default"].AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
	            chart.ChartAreas["Default"].AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
	            chart.ChartAreas["Default"].AxisX.Title = "Время, с\u00d71e-16";
            };
            SetupChartArea(Chart_KE);
            SetupChartArea(Chart_PE);
            SetupChartArea(Chart_TE);
            
            KeyDown += Viewport_Lattice.HandleKeyDown;
            KeyDown += Viewport_SelAtoms.HandleKeyDown;
		}

        void ShowStructure()
        {
            Viewport_Lattice.Clear();
            Viewport_Lattice.CreateDefaultLight();

            foreach (var atom in LatProc.TheLattice.Structure) {
                Viewport_Lattice.Add(Drawer3D.CreateSphere(atom.Position.ToPoint3D(), 0.01, Brushes.Black));
                foreach (var atom2 in atom.FirstNeighbours)
                    Viewport_Lattice.Add(Drawer3D.CreateTube(atom.Position.ToPoint3D(), atom2.Position.ToPoint3D(), 0.002, Brushes.Gray));
            }
            
            Viewport_SelAtoms.Clear();
            Viewport_SelAtoms.CreateDefaultLight();
            
			foreach (AtomInfoPanel entry in LB_SelAtoms.Items) {
	            var pos = entry.RefAtom.Position;
	            Viewport_Lattice.Add(Drawer3D.CreateSphere(pos.ToPoint3D(), 0.015, Brushes.Cyan));
	            Viewport_SelAtoms.Add(Drawer3D.CreateSphere(pos.ToPoint3D(), 0.015, Brushes.Cyan));
            }
            
            if (LB_SelAtoms.SelectedIndex >= 0) {
            	var pos = (LB_SelAtoms.SelectedItem as AtomInfoPanel).RefAtom.Position;
	            Viewport_Lattice.Add(Drawer3D.CreateSphere(pos.ToPoint3D(), 0.02, Brushes.Red));
	            Viewport_SelAtoms.Add(Drawer3D.CreateSphere(pos.ToPoint3D(), 0.02, Brushes.Red));
            }
        }
        
        void PhysRestart()
        {
        	LatProc.KEValues.Clear();
            LatProc.PEValues.Clear();
            EnergyScale = LatProc.TheLattice.PotentialEnergy();

            LatProc.dt = Double.Parse(TB_TimeQuant.Text);
            LatProc.t = -1e16 * LatProc.dt;
            
            Chart_KE.ChartAreas["Default"].AxisX.Minimum = 0;
            Chart_KE.Series.Clear();
            Chart_KE.Series.Add("Main");
            Chart_KE.Series["Main"].ChartArea = "Default";
            Chart_KE.Series["Main"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            
            Chart_PE.ChartAreas["Default"].AxisX.Minimum = 0;
            Chart_PE.ChartAreas["Default"].AxisY.Maximum = 1.1 * EnergyScale - 0.1 * EnergyAsympt;
            Chart_PE.Series.Clear();
            Chart_PE.Series.Add("Main");
            Chart_PE.Series["Main"].ChartArea = "Default";
            Chart_PE.Series["Main"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            Chart_PE.Series.Add("Asympt");
            Chart_PE.Series["Asympt"].ChartArea = "Default";
            Chart_PE.Series["Asympt"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            
            Chart_TE.ChartAreas["Default"].AxisX.Minimum = 0;
            Chart_TE.ChartAreas["Default"].AxisY.Maximum = 1.1 * EnergyScale - 0.1 * EnergyAsympt;
            Chart_TE.Series.Clear();
            Chart_TE.Series.Add("Main");
            Chart_TE.Series["Main"].ChartArea = "Default";
            Chart_TE.Series["Main"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            Chart_TE.Series.Add("Asympt");
            Chart_TE.Series["Asympt"].ChartArea = "Default";
            Chart_TE.Series["Asympt"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                  
            Phys_Calculate(null, null);
            Phys_Calculate(null, null);
        }
		
		void BnGenLattice_Click(object sender, RoutedEventArgs e)
		{
			try {
				double StructSize = Convert.ToDouble(NUD_StructSize.Value);
				var lat = Lattice.SWP_Silica(StructSize);
				EnergyAsympt = lat.PotentialEnergy();
				
				double Shift = Convert.ToDouble(NUD_InitShift.Value);
				var rand = new Random();
				foreach (var atom in lat.Structure) {
					atom.Position.x += Shift * AtomParams.LATPAR_SI * (rand.NextDouble() - 0.5);
					atom.Position.y += Shift * AtomParams.LATPAR_SI * (rand.NextDouble() - 0.5);
					atom.Position.z += Shift * AtomParams.LATPAR_SI * (rand.NextDouble() - 0.5);
				}
	            
                LatProc.TheLattice = lat;
	            LatProc.IsBorderFixed = (bool)Check_FixedBorder.IsChecked;
	            LB_SelAtoms.Items.Clear();
	            if (Check_EnergyDrain.IsChecked == true)
	            	LatProc.RelaxVelocity = (v) => v * Math.Sqrt(1.0 - 0.01 * Convert.ToDouble(NUD_EnergyDrain.Value));
	            else LatProc.RelaxVelocity = (v) => v;
	            
                PhysRestart();
                if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
			} catch (Exception ex) {
				if (ex is FormatException)
					System.Media.SystemSounds.Beep.Play();
			}
		}
		
		void BnRunPhys_Click(object sender, RoutedEventArgs e) 
		{
            if (Bn_RunPhys.IsChecked == true)
            {
                Bn_RunPhys.Content = "Остановить";
                Bn_GenLattice.IsEnabled = false;
                Check_EnergyDrain.IsEnabled = false;
                TB_TimeQuant.IsEnabled = false;
                
                LatProc.dt = Double.Parse(TB_TimeQuant.Text);
                LatProc.Start();
            }
            else
            {
                Bn_RunPhys.Content = "Запустить";
                Bn_GenLattice.IsEnabled = true;
                Check_EnergyDrain.IsEnabled = true;
                TB_TimeQuant.IsEnabled = true;

                LatProc.Stop();
            }
        }
		
		const double ChartWidth = 1000;
		const double DriftThresh = 5e-4;
        void Phys_Calculate(object sender, EventArgs e)
        {
            LatProc.VerletStep();
        	if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
            
            double xmax = LatProc.KEValues.Last().X;
            
            Chart_KE.Series["Main"].Points.AddXY(xmax, LatProc.KEValues.Last().Y);
            
            Chart_PE.Series["Asympt"].Points.AddXY(xmax, EnergyAsympt);
            Chart_PE.Series["Main"].Points.AddXY(xmax, LatProc.PEValues.Last().Y);
            
            Chart_TE.Series["Asympt"].Points.AddXY(xmax, EnergyAsympt);
			var TEValues = LatProc.TEValues;
            Chart_TE.Series["Main"].Points.AddXY(xmax, TEValues.Last().Y);
            
            if (xmax > ChartWidth) {
            	Chart_KE.ChartAreas["Default"].AxisX.Minimum = xmax - ChartWidth;
            	Chart_PE.ChartAreas["Default"].AxisX.Minimum = xmax - ChartWidth;
            	Chart_TE.ChartAreas["Default"].AxisX.Minimum = xmax - ChartWidth;
            	
            	int imax = TEValues.Count - 1;
            	int irange = (int)(ChartWidth * 1e-16 / LatProc.dt);
            	TEValues = TEValues.GetRange(imax - irange, irange);
            }
            
            double TEMax = TEValues.Max((v) => v.Y), TEMin = TEValues.Min((v) => v.Y);
            double EnergyDrift = Math.Abs((TEMax - TEMin) * 2 / (TEMax + TEMin));
            TB_EnergyDrift.Text = String.Format("{0:F2}%", 100 * EnergyDrift);
            TB_EnergyToRelax.Text = String.Format("{0:F2}%", 100 * (1.0 - Math.Abs(LatProc.PEValues.Last().Y / EnergyAsympt)));
            
            if (xmax > ChartWidth && EnergyDrift < DriftThresh) {
            	Bn_RunPhys.IsChecked = false;
            	BnRunPhys_Click(this, null);
            	BnVacEnergy_Click(this, null);
            }
        }
		
		void SearchAtom(Point3D Location, bool Add)
		{
			for (int i = 0; i < LatProc.TheLattice.Structure.Count; i++) {
				if ((new Vector(Location.X, Location.Y, Location.Z) - LatProc.TheLattice.Structure[i].Position).Length < 0.05) {
					int j;
					for (j = 0; j < LB_SelAtoms.Items.Count; j++)
						if ((LB_SelAtoms.Items[j] as AtomInfoPanel).RefId == i)
							break;
					if (j < LB_SelAtoms.Items.Count) {
						if (Add) LB_SelAtoms.SelectedIndex = j;
						else LB_SelAtoms.Items.RemoveAt(j);
						if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
						return;
					}
					LB_SelAtoms.Items.Add(new AtomInfoPanel(LatProc.TheLattice.Structure[i], i));
					if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
					return;
				}
			}
			MessageBox.Show("Атом не найден!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
		}
		
		void BnSearchAtom_Click(object sender, RoutedEventArgs e)
		{
			try {
				SearchAtom(
					new Point3D(
						Convert.ToDouble(NUD_SearchPosX.Value) * AtomParams.LATPAR_SI,
						Convert.ToDouble(NUD_SearchPosY.Value) * AtomParams.LATPAR_SI,
						Convert.ToDouble(NUD_SearchPosZ.Value) * AtomParams.LATPAR_SI
					), true);
			}
			catch (Exception ex) {
				if (ex is NullReferenceException) {
					System.Media.SystemSounds.Beep.Play();
					return;
				}
			}
		}
		
		void BnSelRemove_Click(object sender, RoutedEventArgs e) 
		{
			if (LB_SelAtoms.SelectedIndex < 0) { System.Media.SystemSounds.Beep.Play(); return; }
			var doomedAtom = (LB_SelAtoms.SelectedItem as AtomInfoPanel).RefAtom;
			foreach (var atom in LatProc.TheLattice.Structure) {
				if (atom == doomedAtom) continue;
				atom.FirstNeighbours.Remove(doomedAtom);
				atom.SecondNeighbours.Remove(doomedAtom);
			}
			LatProc.TheLattice.Structure.RemoveAt((LB_SelAtoms.SelectedItem as AtomInfoPanel).RefId);
			LB_SelAtoms.Items.RemoveAt(LB_SelAtoms.SelectedIndex);
			PhysRestart();
			if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
		}
		
		void BnSelNeighFirst_Click(object sender, RoutedEventArgs e)
		{
			if (LB_SelAtoms.SelectedIndex < 0) { System.Media.SystemSounds.Beep.Play(); return; }
			var center = (LB_SelAtoms.SelectedItem as AtomInfoPanel).RefAtom;
			for (int i = 0; i < LatProc.TheLattice.Structure.Count; i++) {
				if (center.FirstNeighbours.Contains(LatProc.TheLattice.Structure[i])) {
					bool found = false;
					foreach (AtomInfoPanel entry in LB_SelAtoms.Items)
						if (entry.RefId == i) { found = true; break; }
					if (!found) LB_SelAtoms.Items.Add(new AtomInfoPanel(LatProc.TheLattice.Structure[i], i));
				}
			}
			if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
		}
		
		void SelAtoms_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
		}
		
		void BnVacEnergy_Click(object sender, RoutedEventArgs e)
		{
			double StructSize = Convert.ToDouble(NUD_StructSize.Value);
			var lat = Lattice.SWP_Silica(StructSize);
			int N0 = lat.Structure.Count;
			int NV = LatProc.TheLattice.Structure.Count;
			double Ev = EnergyAsympt * NV / N0 - LatProc.PEValues.Last().Y;
			MessageBox.Show("Энергия образования вакансии: " + Ev);
		}
		
		void CheckRender_Click(object sender, RoutedEventArgs e)
		{
			if ((bool)Check_RenderLattice.IsChecked) ShowStructure();
		}
	}
}