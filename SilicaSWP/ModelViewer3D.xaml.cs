﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 17.11.2021
 * Time: 9:16
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SilicaSWP
{
	/// <summary>
	/// Interaction logic for ModelViewer3D.xaml
	/// </summary>
	public partial class ModelViewer3D : UserControl
	{
		double _phi = 0, _theta = 0, _r = 5;
		public double CameraPhi {
			get { return _phi; }
			set { _phi = value; PositionCamera(); }
		}
		public double CameraTheta {
			get { return _theta; }
			set { _theta = value; PositionCamera(); }
		}
		double CameraR {
			get { return _r; }
			set { _r = value; PositionCamera(); }
		}
		const double CameraDPhi = Math.PI / 16.0, CameraDTheta = Math.PI / 16.0, CameraDR = 0.5;
		
		public delegate void SearchAtom(Point3D Location, bool Add);
		public event SearchAtom SearchAtomEvent;
		
		public ModelViewer3D()
		{
			InitializeComponent();
			PositionCamera();
		}
		
		public void Clear() { Viewport.Children.Clear(); }
		public void Add(ModelVisual3D Model) { Viewport.Children.Add(Model); }
		
		public void CreateDefaultLight() {
			Add(new ModelVisual3D() { Content = new DirectionalLight(Colors.White, new Vector3D(-1, -1, -1)) });
			Add(new ModelVisual3D() { Content = new DirectionalLight(Colors.White, new Vector3D( 1,  1,  1)) });
		}
		
		public void HandleKeyDown(object sender, KeyEventArgs e)
		{
			switch (e.Key)
			{
				//case Key.Right:
				case Key.D:
					CameraPhi += CameraDPhi;
					break;
				//case Key.Left:
				case Key.A:
					CameraPhi -= CameraDPhi;
					break;
				//case Key.Up:
				case Key.W:
					if (CameraTheta > -0.5 * Math.PI)
						CameraTheta -= CameraDTheta;
					break;
				//case Key.Down:
				case Key.S:
					if (CameraTheta < 0.5 * Math.PI)
						CameraTheta += CameraDTheta;
					break;
				//case Key.Add:
				//case Key.OemPlus:
				case Key.Q:
					CameraR -= CameraDR;
					if (CameraR < CameraDR) CameraR = CameraDR;
					break;
				//case Key.Subtract:
				//case Key.OemMinus:
				case Key.E:
					CameraR += CameraDR;
					break;
			}
		}
		
		// Position the camera.
		void PositionCamera()
		{
			// Calculate the camera's position in Cartesian coordinates.
			double z = CameraR * Math.Sin(CameraTheta);
			double hyp = CameraR * Math.Cos(CameraTheta);
			double x = hyp * Math.Cos(CameraPhi);
			double y = hyp * Math.Sin(CameraPhi);
			TheCamera.Position = new Point3D(x, y, z);

			// Look toward the origin.
			TheCamera.LookDirection = new Vector3D(-x, -y, -z);
		}
		
		void Viewport_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed) {
				var pos = e.GetPosition(sender as Viewport3D);
				//var testpt = new Point3D(pos.X, pos.Y, 0);
				//var testdir = new Vector3D(pos.X, pos.Y, 10);
				var pointtestparams = new PointHitTestParameters(pos);
				//var raytestparams = new RayHitTestParameters(testpt, testdir);
				VisualTreeHelper.HitTest(sender as Viewport3D, null, HTResultAdd, pointtestparams);
			}
			else if (e.RightButton == MouseButtonState.Pressed) {
				var pos = e.GetPosition(sender as Viewport3D);
				var pointtestparams = new PointHitTestParameters(pos);
				VisualTreeHelper.HitTest(sender as Viewport3D, null, HTResultRemove, pointtestparams);
			}
		}
		
		HitTestResultBehavior HTResultAdd(HitTestResult rawresult)
		{
			var ray = rawresult as RayHitTestResult;
			if (ray != null) {
				var rayMesh = ray as RayMeshGeometry3DHitTestResult;
				if (rayMesh != null) {
					var hitgeo = rayMesh.ModelHit as GeometryModel3D;
					if (hitgeo.Bounds.SizeX * hitgeo.Bounds.SizeY * hitgeo.Bounds.SizeZ < 1e-4) {
						SearchAtomEvent.Invoke(hitgeo.Bounds.Location, true);
					}
				}
			}
			return HitTestResultBehavior.Stop;
		}
		
		HitTestResultBehavior HTResultRemove(HitTestResult rawresult)
		{
			var ray = rawresult as RayHitTestResult;
			if (ray != null) {
				var rayMesh = ray as RayMeshGeometry3DHitTestResult;
				if (rayMesh != null) {
					var hitgeo = rayMesh.ModelHit as GeometryModel3D;
					if (hitgeo.Bounds.SizeX * hitgeo.Bounds.SizeY * hitgeo.Bounds.SizeZ < 1e-4) {
						SearchAtomEvent.Invoke(hitgeo.Bounds.Location, false);
					}
				}
			}
			return HitTestResultBehavior.Stop;
		}
	}
}