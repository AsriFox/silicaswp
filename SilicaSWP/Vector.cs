﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 09/12/2021
 * Time: 14:29
 */
using System;

namespace SilicaSWP
{
	/// <summary>
	/// Описание класса "Вектор".
	/// </summary>
	public class Vector
	{
		/// <summary>
		/// Трёхмерные декартовы координаты вектора.
		/// </summary>
		public double x, y, z;
		
		/// <summary>
		/// Стандартный конструктор по координатам.
		/// </summary>
		public Vector(double x, double y, double z) { this.x = x; this.y = y; this.z = z; }
		
		/// <summary>
		/// Постоянный нуль-вектор.
		/// </summary>
		public static Vector Zero { get { return new Vector(0, 0, 0); } }
		
		/// <summary>
		/// Квадрат длины вектора.
		/// </summary>
		public double LengthSquared { get { return x * x + y * y + z * z; } }
		
		/// <summary>
		/// Длина вектора.
		/// </summary>
		public double Length { get { return Math.Sqrt(LengthSquared); } }
		
		/// <summary>
		/// Нормализованный вектор.
		/// </summary>
		public Vector Normalized { get { return this / this.Length; } }
		
		/// <summary>
        /// Вычисление расстояния между атомами с учётом кубических периодических граничных условий.
        /// </summary>
        /// <param name="vector1">Координата первого атома.</param>
        /// <param name="vector2">Координата второго атома.</param>
        /// <param name="L">Размер кубической расчётной ячейки.</param>
        /// <returns>Значение функции.</returns>
        public static double DistancePeriodic(Vector vector1, Vector vector2, double L)
        {
            double dx = vector1.x - vector2.x;
            if (Math.Abs(dx) > 0.5 * L) 
            	dx -= Math.Sign(dx) * L;

            double dy = vector1.y - vector2.y;
            if (Math.Abs(dy) > 0.5 * L) 
            	dy -= Math.Sign(dy) * L;

            double dz = vector1.z - vector2.z;
            if (Math.Abs(dz) > 0.5 * L) 
            	dz -= Math.Sign(dz) * L;

            double l2 = (vector1 - vector2).Length;
            return Math.Sqrt(dx * dx + dy * dy + dz * dz);
        }
        
        public System.Windows.Media.Media3D.Point3D ToPoint3D() {
        	return new System.Windows.Media.Media3D.Point3D(x, y, z);
        }
        
        public static Vector operator -(Vector v) {
        	return new Vector(-v.x, -v.y, -v.z);
        }
        
        public static Vector operator +(Vector v1, Vector v2) {
        	return new Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }
        
        public static Vector operator -(Vector v1, Vector v2) {
        	return new Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }
        
        public static Vector operator *(Vector v, double k) {
        	return new Vector(k * v.x, k * v.y, k * v.z);
        }
        
        public static Vector operator *(double k, Vector v) {
        	return new Vector(k * v.x, k * v.y, k * v.z);
        }
        
        public static Vector operator /(Vector v, double k) {
        	return new Vector(v.x / k, v.y / k, v.z / k);
        }
        
        /// <summary>
        /// Скалярное произведение векторов.
        /// </summary>
        public static double operator *(Vector v1, Vector v2) {
        	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        }
        
        /// <summary>
        /// Векторное произведение векторов.
        /// </summary>
        public static Vector operator %(Vector v1, Vector v2) {
        	return new Vector(v1.y * v2.z - v2.y * v1.z, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
        }
	}
}
