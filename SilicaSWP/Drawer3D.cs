﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 13.09.2021
 * Time: 8:17
 */
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SilicaSWP
{
	/// <summary>
	/// Класс функций трёхмерного рисования.
	/// </summary>
	public static class Drawer3D
	{
		/// <summary>
		/// Генератор трёхмерной модели куба.
		/// </summary>
		/// <param name="Center">Положение центра фигуры.</param>
		/// <param name="Radius">Размер фигуры.</param>
		/// <param name="ColorBrush">Цветная кисть для материала.</param>
		/// <returns>Модель куба для вставки в Viewport3D.</returns>
		public static ModelVisual3D CreateCube(Point3D Center, double Radius, Brush ColorBrush)
		{
			var corners = new Point3D[8] {
				new Point3D(-1, -1, -1), new Point3D(+1, -1, -1),
				new Point3D(-1, +1, -1), new Point3D(+1, +1, -1),
				new Point3D(-1, -1, +1), new Point3D(+1, -1, +1),
				new Point3D(-1, +1, +1), new Point3D(+1, +1, +1)
			};
			var indices = new int[36] {
				2, 3, 1,	2, 1, 0,	7, 1, 3,	7, 5, 1,
				6, 5, 7,	6, 4, 5,	6, 2, 0,	6, 0, 4,
				2, 7, 3,	2, 6, 7,	0, 1, 5,	0, 5, 4,
			};
			var Mesh = new MeshGeometry3D {
				Positions = new Point3DCollection(corners),
				TriangleIndices = new Int32Collection(indices)
			};
			
			var morph = new Transform3DGroup();
			morph.Children.Add(new ScaleTransform3D(Radius, Radius, Radius));
			morph.Children.Add(new TranslateTransform3D(Center.X, Center.Y, Center.Z));
			
			return new ModelVisual3D {
				Content = new GeometryModel3D(Mesh, new DiffuseMaterial(ColorBrush)) { Transform = morph }
			};
		}
		
		/// <summary>
		/// Генератор трёхмерной модели цилиндра.
		/// </summary>
		/// <param name="P1">Положение первого конца.</param>
		/// <param name="P2">Положение второго конца.</param>
		/// <param name="Radius">Радиус торцевой окружности.</param>
		/// <param name="ColorBrush">Цветная кисть для материала.</param>
		/// <returns>Модель цилиндра для вставки в Viewport3D.</returns>
		public static ModelVisual3D CreateTube(Point3D P1, Point3D P2, double Radius, Brush ColorBrush)
		{
			const int N = 4;
			var corners = new Point3D[2 * N];
			var indices = new List<int>();
			
			var Axis = new Vector(P2.X - P1.X, P2.Y - P1.Y, P2.Z - P1.Z);
			var RotAx = new Vector(1, 0, 0) % Axis;
			
			corners[0] = new Point3D(0, Radius, 0);
			corners[1] = new Point3D(Axis.Length, Radius, 0);
				
			const double dphi = Math.PI * 2.0 / N;
			for (int i = 1; i < N; i++) {
				double y = Radius * Math.Cos(i * dphi);
				double z = Radius * Math.Sin(i * dphi);
				
				corners[2 * i] = new Point3D(0, y, z);
				corners[2 * i + 1] = new Point3D(Axis.Length, y, z);
				
				indices.Add(2 * i);	indices.Add(2 * i - 1);	indices.Add(2 * i - 2);
				indices.Add(2 * i);	indices.Add(2 * i + 1);	indices.Add(2 * i - 1);
			}
			
			indices.Add(2 * N - 2);	indices.Add(1); 		indices.Add(0);
			indices.Add(2 * N - 2);	indices.Add(2 * N - 1);	indices.Add(1);
			
			var Mesh = new MeshGeometry3D {
				Positions = new Point3DCollection(corners),
				TriangleIndices = new Int32Collection(indices)
			};
			
			var morph = new Transform3DGroup();
			morph.Children.Add(new RotateTransform3D(
				new AxisAngleRotation3D(
					new Vector3D(RotAx.x, RotAx.y, RotAx.z), 
					Math.Acos(Axis.x / Axis.Length) * 180.0 / Math.PI)));
			morph.Children.Add(new TranslateTransform3D(P1.X, P1.Y, P1.Z));
			
			return new ModelVisual3D {
				Content = new GeometryModel3D(Mesh, new DiffuseMaterial(ColorBrush)) { Transform = morph }
			};
		}
		
		/// <summary>
		/// Константа золотого сечения ф = (1 + sqrt(5)) / 2.
		/// </summary>
		public const double GOLD_PHI = 1.618033989;
		
		/// <summary>
		/// Коэффициент нормировки радиуса икосаэдра k = 1 / r = 1 / sqrt(1 + GOLD_PHI ^ 2).
		/// </summary>
		public const double ICOS_RAD = 0.525731112;
		
		/// <summary>
		/// Генератор трёхмерной модели икосаэдрической сферы.
		/// </summary>
		/// <param name="Center">Положение центра фигуры.</param>
		/// <param name="Radius">Размер фигуры.</param>
		/// <param name="ColorBrush">Цветная кисть для материала.</param>
		/// <returns>Модель сферы для вставки в Viewport3D.</returns>
		public static ModelVisual3D CreateSphere(Point3D Center, double Radius, Brush ColorBrush)
		{
			var corners = new Point3D[12] {
				new Point3D(-ICOS_RAD,  ICOS_RAD * GOLD_PHI, 0), new Point3D(ICOS_RAD,  ICOS_RAD * GOLD_PHI, 0),
				new Point3D(-ICOS_RAD, -ICOS_RAD * GOLD_PHI, 0), new Point3D(ICOS_RAD, -ICOS_RAD * GOLD_PHI, 0),
				new Point3D(0, -ICOS_RAD,  ICOS_RAD * GOLD_PHI), new Point3D(0, ICOS_RAD,  ICOS_RAD * GOLD_PHI),
				new Point3D(0, -ICOS_RAD, -ICOS_RAD * GOLD_PHI), new Point3D(0, ICOS_RAD, -ICOS_RAD * GOLD_PHI),
				new Point3D( ICOS_RAD * GOLD_PHI, 0, -ICOS_RAD), new Point3D( ICOS_RAD * GOLD_PHI, 0, ICOS_RAD),
				new Point3D(-ICOS_RAD * GOLD_PHI, 0, -ICOS_RAD), new Point3D(-ICOS_RAD * GOLD_PHI, 0, ICOS_RAD)
			};
			
			var indices = new int[60] {
				0, 11, 5,	0, 5, 1,	0, 1, 7,	0, 7, 10,	0, 10, 11,
				1, 5, 9,	5, 11, 4,	11, 10, 2,	10, 7, 6,	7, 1, 8,
				3, 9, 4,	3, 4, 2,	3, 2, 6,	3, 6, 8,	3, 8, 9,
				4, 9, 5,	2, 4, 11,	6, 2, 10,	8, 6, 7,	9, 8, 1
			};
			
			var Mesh = new MeshGeometry3D {
				Positions = new Point3DCollection(corners),
				TriangleIndices = new Int32Collection(indices)
			};
			
			var morph = new Transform3DGroup();
			morph.Children.Add(new ScaleTransform3D(Radius, Radius, Radius));
			morph.Children.Add(new TranslateTransform3D(Center.X, Center.Y, Center.Z));
			
			return new ModelVisual3D {
				Content = new GeometryModel3D(Mesh, new DiffuseMaterial(ColorBrush)) { Transform = morph }
			};
		}
	}
}
