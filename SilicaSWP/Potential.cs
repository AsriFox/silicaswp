﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 09/12/2021
 * Time: 13:55
 */
using System;
using System.Linq;

namespace SilicaSWP
{
	public abstract class Potential
	{
		public abstract double PotentialEnergy(Atom atom);
		public abstract Vector PForce(Atom atom, double dl);
	}
	
	public class PotentialStWb : Potential 
	{
        /// <summary>
        /// Параметры потенциала Стиллинджера-Вебера.
        /// </summary>
		public double A, B, p, q, s, e, sigma, lambda, gamma;
		
		/// <summary>
		/// Конструктор потенциала Стиллинджера-Вебера для взаимодействия атомов кремния (Si).
		/// </summary>
		/// <returns>Объект класса.</returns>
		public static PotentialStWb SWP_Silica() 
		{
            const double si = 0.2095;
            
			return new PotentialStWb {
				A = 7.049556277,
				B = 0.6022245584,
				p = 4,
				q = 0,
				s = si,
				e = 2.1672381,
				sigma = 1.80 * si,
				lambda = 21.0,
				gamma = 1.20
            };
		}
		
		/// <summary>
        /// Функция двухчастичного взаимодействия ф(r).
        /// </summary>
        /// <param name="r">Расстояние до атома-соседа.</param>
        /// <returns>Значение функции.</returns>
        private double PotFunc_2P(double r)
        {
            if (r < sigma) 
            	return e * A * (B * Math.Pow(s / r, p) - Math.Pow(s / r, q)) * Math.Exp(s / (r - sigma));
            return 0.0;
        }
		
        /// <summary>
        /// Радиальная функция трёхатомного взаимодействия, критический радиус которой находится между первым и вторым ближайшим соседом.
        /// </summary>
        /// <param name="Rij">Расстояние до первого соседа.</param>
        /// <param name="Rik">Расстояние до второго соседа.</param>
        /// <returns>Значение функции</returns>
        private double PotFunc_3PR(double Rij, double Rik)
        {
        	if (Rij < sigma && Rik < sigma)
        		return e * lambda * Math.Exp((gamma * s / (Rij - sigma)) + (gamma * s / (Rik - sigma)));
        	return 0.0;
        }
        
        /// <summary>
        /// Функция квадрата косинуса* между связями в трёхатомном взаимодействии.
        /// </summary>
        /// <param name="Rij">Расстояние до первого соседа.</param>
        /// <param name="Rik">Расстояние до второго соседа.</param>
        /// <param name="Rjk">Расстояние между соседями.</param>
        /// <returns>Значение функции.</returns>
        private double PotFunc_Cos(double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i-j и i-k вычисляется по теореме косинусов:
            double cosTheta = (Rik * Rik + Rij * Rij - Rjk * Rjk) / (2.0 * Rij * Rik);
            return ((1.0 / 3.0) + cosTheta) * ((1.0 / 3.0) + cosTheta);
        }
        
        /// <summary>
        /// Функция потенциальной энергии атома.
        /// </summary>
        /// <param name="atom">Атом-субъект.</param>
        /// <returns>Значение функции.</returns>
        public override double PotentialEnergy(Atom atom) 
        {
        	double Energy_2P = 0.0, Energy_3P = 0.0;
        	
        	// var Neighbours = atom.FirstNeighbours.Concat(atom.SecondNeighbours);
        	foreach (Atom AJ in atom.SecondNeighbours) {
                double Rij = (atom.Position - AJ.Position).Length;
                Energy_2P += PotFunc_2P(Rij);
        		
        		foreach (Atom AK in atom.SecondNeighbours) {
        			if (AJ == AK) continue;

                    double Rik = (atom.Position - AK.Position).Length;
                    double Rjk = (AJ.Position - AK.Position).Length;

        			Energy_3P += PotFunc_3PR(Rij, Rik) * PotFunc_Cos(Rij, Rik, Rjk);
        		}
        	}
        	
        	return Energy_2P / 2.0 + Energy_3P / 3.0;
        }
        
        /// <summary>
        /// Функция силы, поле которой определяется потенциальной энергией.
        /// </summary>
        /// <param name="atom">Атом-субъект.</param>
        /// <param name="dl">Пространственный шаг дифференцирования.</param>
        /// <returns>Значение функции.</returns>
		public override Vector PForce(Atom atom, double dl)
		{
			var atomUpper = atom.Clone();
			var atomLower = atom.Clone();
			
			atomUpper.Position = new Vector(atom.Position.x + dl, atom.Position.y, atom.Position.z);
            atomLower.Position = new Vector(atom.Position.x - dl, atom.Position.y, atom.Position.z);
			double Fx = (PotentialEnergy(atomLower) - PotentialEnergy(atomUpper)) / (2.0 * dl);

            atomUpper.Position = new Vector(atom.Position.x, atom.Position.y + dl, atom.Position.z);
            atomLower.Position = new Vector(atom.Position.x, atom.Position.y - dl, atom.Position.z);
			double Fy = (PotentialEnergy(atomLower) - PotentialEnergy(atomUpper)) / (2.0 * dl);

			atomUpper.Position = new Vector(atom.Position.x, atom.Position.y, atom.Position.z + dl);
            atomLower.Position = new Vector(atom.Position.x, atom.Position.y, atom.Position.z - dl);
			double Fz = (PotentialEnergy(atomLower) - PotentialEnergy(atomUpper)) / (2.0 * dl);
			
			return new Vector(Fx, Fy, Fz) / AtomParams.JtoEV;
		}
	}
}
