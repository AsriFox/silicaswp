﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 09/12/2021
 * Time: 14:20
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SilicaSWP
{
	/// <summary>
	/// Известные параметры атомов.
	/// </summary>
	public static class AtomParams 
	{
		/// <summary>
		/// Константа перевода атомных единиц массы в килограммы.
		/// </summary>
		public const double AMUtoKG = 1.66054e-27;
		
		/// <summary>
		/// Масса атома кремния, кг.
		/// </summary>
		public const double MASS_SI = 28.085 * AMUtoKG;
		
        /// <summary>
        /// Параметр решётки атома кремния, нм.
        /// </summary>
        public const double LATPAR_SI = 0.54307;
        
        /// <summary>
        /// Постоянная Больцмана, эВ/К.
        /// </summary>
        public const double BOLTZMANN = 8.6142e-5;

        /// <summary>
        /// Период оптических колебаний атомов в решётке сплава, с.
        /// </summary>
        public const double OPTOSC_PERIOD = 1.018e-13;
        
        /// <summary>
        /// Константа перевода аттоджоулей (E-18 ~ (нм/c)^2) в электронвольты.
        /// </summary>
        public const double JtoEV = 6.24151;
	}
	
	/// <summary>
	/// Описание класса "Атом".
	/// </summary>
	public class Atom : INotifyPropertyChanged
	{
		/// <summary>
		/// Радиус-вектор положения атома.
		/// </summary>
		Vector _position;
		public Vector Position {
			get { return _position; }
			set { _position = value;
				NotifyPropertyChanged();
			}
		}
		
		/// <summary>
		/// Вектор скорости атома.
		/// </summary>
		Vector _velocity;
		public Vector Velocity {
			get { return _velocity; }
			set { _velocity = value;
				NotifyPropertyChanged();
			}
		}
		
		/// <summary>
		/// Масса атома.
		/// </summary>
		public double Mass;
		
		/// <summary>
		/// Список первых соседей атома.
		/// </summary>
		public List<Atom> FirstNeighbours;
		
		/// <summary>
		/// Список вторых соседей атома.
		/// </summary>
		public List<Atom> SecondNeighbours;

        /// <summary>
        /// Признак граничного атома.
        /// </summary>
        public bool IsBorder = false;
		
		public Atom() {
			FirstNeighbours = new List<Atom>();
			SecondNeighbours = new List<Atom>();
		}
		
		public static Atom Si(Vector Pos) {
			return new Atom {
				Position = Pos,
				Velocity = Vector.Zero,
				Mass = AtomParams.MASS_SI
			};
		}
        
        public Atom Clone() {
        	return new Atom {
        		Position = Position,
        		Velocity = Velocity,
        		Mass = Mass,
        		FirstNeighbours = FirstNeighbours,
        		SecondNeighbours = SecondNeighbours,
        		IsBorder = IsBorder
        	};
        }
		
		/// <summary>
		/// Функция кинетической энергии атома.
		/// </summary>
		public double KineticEnergy { get { return 0.5 * Velocity.LengthSquared * Mass * AtomParams.JtoEV; } }

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;
		
		void NotifyPropertyChanged([CallerMemberName] String propertyName = "") {
			if (PropertyChanged != null)
				PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
