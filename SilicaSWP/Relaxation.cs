﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 09/12/2021
 * Time: 15:48
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace SilicaSWP
{
	/// <summary>
	/// Описание класса "Релаксационный процесс".
	/// </summary>
	public class Relaxation : DispatcherTimer
	{
		public Lattice TheLattice;
		public bool IsBorderFixed;
		public double t, dt;
		public List<Point> KEValues, PEValues, TEValues;
		
		public Relaxation()
		{
			KEValues = new List<Point>();
			PEValues = new List<Point>();
			TEValues = new List<Point>();
			
			Interval = TimeSpan.FromMilliseconds(2);
		}
		
		public Func<Vector, Vector> RelaxVelocity;

		/// <summary>
		/// Исполнение алгоритма Верле в скоростной форме на одном (следующем) временном шаге.
		/// </summary>
		public void VerletStep() 
		{
			foreach (var atom in TheLattice.Structure) {
				if (!(atom.IsBorder && IsBorderFixed)) {
					var ForceOld = TheLattice.PotFunc.PForce(atom, AtomParams.LATPAR_SI * 0.01);
					atom.Position += atom.Velocity * dt + ForceOld * 0.5 * dt * dt / atom.Mass;
	
	                var ForceNew = TheLattice.PotFunc.PForce(atom, AtomParams.LATPAR_SI * 0.01);
	                atom.Velocity += 0.5 * (ForceOld + ForceNew) * dt / atom.Mass;
				}
			}
			t += 1e16 * dt;
			
			try {
				double KE = TheLattice.KineticEnergy();
				int k = KEValues.Count;
				if (KE < KEValues[k - 1].Y && KEValues[k - 1].Y > KEValues[k - 2].Y)
					TheLattice.Structure.ForEach(atom => atom.Velocity = RelaxVelocity(atom.Velocity));
				// TheLattice.Structure.AsParallel().ForAll<Atom>(atom => atom.Velocity = RelaxVelocity(atom.Velocity));
			}
			catch (ArgumentOutOfRangeException) {}
			KEValues.Add(new Point(t, TheLattice.KineticEnergy()));
			PEValues.Add(new Point(t, TheLattice.PotentialEnergy()));
			TEValues.Add(new Point(t, KEValues.Last().Y + PEValues.Last().Y));
		}
	}
}
