﻿/*
 * Created by SharpDevelop.
 * User: Азриэль
 * Date: 09/12/2021
 * Time: 15:14
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace SilicaSWP
{
	/// <summary>
	/// Описание класса "Кристаллическая решётка".
	/// </summary>
	public class Lattice
	{
		/// <summary>
		/// Параметр кубической кристаллической решётки.
		/// </summary>
		public double LatParam;
		
		/// <summary>
		/// Размер структуры.
		/// </summary>
		public double StructSize;
		
		/// <summary>
		/// Потенциал взаимодействия атомов решётки.
		/// </summary>
		public Potential PotFunc;
		
		/// <summary>
		/// Коллекция атомов решётки.
		/// </summary>
		public List<Atom> Structure;
		
		/// <summary>
		/// Заполнение структуры атомами кремния.
		/// </summary>
		/// <param name="StructSize">Размер структуры.</param>
		public static List<Atom> GetStructure_Silica(double StructSize) 
		{
			var Structure = new List<Atom>();
			
			// Создание атомов:
			for (double x = -0.5 * StructSize; x <= 0.5 * StructSize; x += 1.0)
				for (double y = -0.5 * StructSize; y <= 0.5 * StructSize; y += 1.0)
					for (double z = -0.5 * StructSize; z <= 0.5 * StructSize; z += 1.0) 
					{
						const double k = 0.25 * AtomParams.LATPAR_SI;
						Vector CellPos = new Vector(x, y, z) * AtomParams.LATPAR_SI;
						
						Structure.Add(Atom.Si(CellPos));
						Structure.Add(Atom.Si(CellPos + k * new Vector(0, 2, 2)));
						Structure.Add(Atom.Si(CellPos + k * new Vector(2, 0, 2)));
						Structure.Add(Atom.Si(CellPos + k * new Vector(2, 2, 0)));
						Structure.Add(Atom.Si(CellPos + k * new Vector(1, 3, 3)));
						Structure.Add(Atom.Si(CellPos + k * new Vector(3, 1, 3)));
						Structure.Add(Atom.Si(CellPos + k * new Vector(3, 3, 1)));
						Structure.Add(Atom.Si(CellPos + k * new Vector(1, 1, 1)));
					}
			Structure.RemoveAll((atom) =>
			                       Math.Abs(atom.Position.x) > 0.5 * StructSize * AtomParams.LATPAR_SI
			                    || Math.Abs(atom.Position.y) > 0.5 * StructSize * AtomParams.LATPAR_SI
			                    || Math.Abs(atom.Position.z) > 0.5 * StructSize * AtomParams.LATPAR_SI);
			
			// Заполнение списков соседей:
			const double LinkRadius = AtomParams.LATPAR_SI * 0.4331;	// Радиус поиска ближних соседей по кристаллу;
			const double NeighRadius = AtomParams.LATPAR_SI * 1.040658;	// Радиус поиска = 1.5х радиус обрезания потенциала;

            foreach (var atom in Structure)
            {
                foreach (var atom2 in Structure)
                {
                    if (atom == atom2) continue;

                    if ((atom.Position - atom2.Position).Length <= LinkRadius)
                        atom.FirstNeighbours.Add(atom2);
                    if ((atom.Position - atom2.Position).Length <= NeighRadius)
                        atom.SecondNeighbours.Add(atom2);
                }

                if (atom.FirstNeighbours.Count < 4)
                    atom.IsBorder = true;
            }
			
			return Structure;
		}
		
		/// <summary>
		/// Конструктор решётки кремния с потенциалом Стиллинджера-Вебера.
		/// </summary>
		public static Lattice SWP_Silica(double Size)
		{
			return new Lattice {
				LatParam = AtomParams.LATPAR_SI,
				StructSize = Size,
				PotFunc = PotentialStWb.SWP_Silica(),
				Structure = GetStructure_Silica(Size)
			};
		}
		
		/// <summary>
		/// Расчёт суммарной потенциальной энергии атомов структуры.
		/// </summary>
		public double PotentialEnergy() 
		{
//			double PETotal = 0.0;
//			double L = StructSize * LatParam;
//			Structure.AsParallel().Sum(atom => atom.IsBorder ? 0 : PotFunc.PotentialEnergy(atom));
//			foreach (Atom atom in Structure)
//                if (!atom.IsBorder)
//				    PETotal += PotFunc.PotentialEnergy(atom);
//			return PETotal;
			return Structure.AsParallel().Sum(
				atom => atom.IsBorder ? 0 : PotFunc.PotentialEnergy(atom)
			);
		}
		
		/// <summary>
		/// Расчёт суммарной кинетической энергии атомов структуры.
		/// </summary>
		public double KineticEnergy()
		{
//			double KETotal = 0.0;
//			foreach (Atom atom in Structure)
//                if (!atom.IsBorder)
//					KETotal += atom.KineticEnergy;
//			return KETotal;
			return Structure.AsParallel().Sum(
				atom => atom.IsBorder ? 0 : atom.KineticEnergy
			);
		}
		
		/// <summary>
		/// Энергия равновесного состояния структуры, эВ.
		/// </summary>
		/// <param name="T">Температура структуры.</param>
		public double EquilibriumEnergy(double T) {
			return 1.5 * AtomParams.BOLTZMANN * Structure.Count * T;
		}
	}
}
